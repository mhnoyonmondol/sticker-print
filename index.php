<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sticker print test</title>
</head>
<body>
<h1>Sticker print test</h1>
<?php
    require_once 'vendor/autoload.php';


    use Mike42\Escpos\Printer;
    use Mike42\Escpos\PrintConnectors\FilePrintConnector;
    use Mike42\Escpos\CapabilityProfile;
    use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;


    $connector = new WindowsPrintConnector("ZDesigner GT800(EPL)");
    $printer = new Printer($connector);

    $printer -> text("hello world");
    $printer -> text("\n");
    $printer -> text("\n");
    $printer -> text("hello again");
//    $printer -> cut();
    $printer -> close();

?>
</body>
</html>